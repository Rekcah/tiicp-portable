import os


class Convert():
    @staticmethod
    def diytojson(diy_name):
        if not os.path.isfile(diy_name):
            pass  # TODO gestione errori
            print("ERR")
        else:
            diy_file = open(diy_name, "r")
            diy_body = diy_file.readlines()
            diy_json = {'titolo': '', 'elementi': [], 'pdf': ''}
            titolo_check = False
            elementi_check = False
            pdf_check = False
            for line in diy_body:
                line = line.replace('\n', '')
                if '[titolo]' == line.lower():
                    titolo_check = True
                elif '[elementi]' == line.lower():
                    titolo_check = False
                    elementi_check = True
                elif '[pdf]' == line.lower():
                    elementi_check = False
                    pdf_check = True

                if line != "\n" and line != '' and line not in ['[titolo', '[elementi]', '[pdf]']:
                    if titolo_check is True:
                        diy_json['titolo'] = line
                    elif elementi_check is True:
                        diy_json['elementi'].append(line)
                    elif pdf_check is True:
                        diy_json['pdf'] = line

            return diy_json
