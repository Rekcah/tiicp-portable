import os
import tkinter
from tkinter import ttk

from buttons.results import Results
from settings import Settings


class showProductsFound():

    def __init__(self, products):
        showproductsfound_window = tkinter.Toplevel()
        showproductsfound_window.title(Settings.get_setting_data("title"))
        window_width = Settings.get_setting_data("width")
        window_height = Settings.get_setting_data("height")
        screen_width = showproductsfound_window.winfo_screenwidth()
        screen_height = showproductsfound_window.winfo_screenheight()
        center_x = int(screen_width / 2 - window_width / 2)
        center_y = int(screen_height / 2 - window_height / 2)
        showproductsfound_window.geometry(f'{window_width}x{window_height}+{center_x}+{center_y}')
        showproductsfound_window.configure(bg='black')

        notebook = ttk.Notebook(showproductsfound_window)
        notebook.pack(pady=10, expand=True)

        def esplora(product):
            os.system(Settings.get_setting_data('pdf_command') + Results.results(product, 'pdf'))

        def back():
            showproductsfound_window.destroy()

        frames = []
        for product in products:
            locals()[product] = ttk.Frame(notebook, width=1000)
            locals()[product].pack(fill='both', expand=True)
            notebook.add(locals()[product], text=product)
            ttk.Label(locals()[product], text="Ingredienti:").grid(column=0, row=0)
            product_details = "· " + "\n· ".join(map(str, Results.results(product, 'details')))
            ttk.Label(locals()[product], text=product_details, state="readonly").grid(column=1, row=1)
            tkinter.Button(locals()[product], text="Esplora", command=lambda product=product: esplora(product)).grid(
                column=0, row=2)
            tkinter.Button(locals()[product], text="Indietro", command=back).grid(column=1, row=2)

        showproductsfound_window.mainloop()
