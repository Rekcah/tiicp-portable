import os
import tkinter
from tkinter import ttk

from buttons.results import Results
from settings import Settings

class showProductsFromIngredients():

    def __init__(self, diy_found):
        showingredientsfound_window = tkinter.Toplevel()
        showingredientsfound_window.title(Settings.get_setting_data("title"))
        window_width = Settings.get_setting_data("width")
        window_height = Settings.get_setting_data("height")
        screen_width = showingredientsfound_window.winfo_screenwidth()
        screen_height = showingredientsfound_window.winfo_screenheight()
        center_x = int(screen_width / 2 - window_width / 2)
        center_y = int(screen_height / 2 - window_height / 2)
        showingredientsfound_window.geometry(f'{window_width}x{window_height}+{center_x}+{center_y}')
        showingredientsfound_window.configure(bg='black')

        notebook = ttk.Notebook(showingredientsfound_window)
        notebook.pack(pady=10, expand=True)

        def esplora(ingredient):
            os.system(Settings.get_setting_data('pdf_command') + Results.results(ingredient, 'pdf'))

        def back():
            showingredientsfound_window.destroy()

        # frames = []
        # for ingredient in ingredients:
        #     locals()[ingredient] = ttk.Frame(notebook, width=1000)
        #     locals()[ingredient].pack(fill='both', expand=True)
        #     notebook.add(locals()[ingredient], text=ingredient)
        #     ttk.Label(locals()[ingredient], text="Ingredienti:").grid(column=0, row=0)
        #     ingredient_details = "· " + "\n· ".join(map(str, Results.results(ingredient, 'details')))
        #     ttk.Label(locals()[ingredient], text=ingredient_details, state="readonly").grid(column=1, row=1)
        #     tkinter.Button(locals()[ingredient], text="Esplora", command=lambda ingredient=ingredient: esplora(ingredient)).grid(
        #         column=0, row=2)
        #     tkinter.Button(locals()[ingredient], text="Indietro", command=back).grid(column=1, row=2)

        showingredientsfound_window.mainloop()