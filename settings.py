import json

setting_file = "settings.conf"


class Settings:
    @staticmethod
    def get_setting_data(field):
        with open(setting_file, "r") as json_data_file:
            data = json.load(json_data_file)
        return data[field]
