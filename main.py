import tkinter
from tkinter import messagebox
from settings import Settings
from buttons.cerca import Cerca


class TIICP():
    @staticmethod
    def event_test():   #TODO test
        return messagebox.showinfo('Test', 'This is a test!')

    @staticmethod
    def cerca():
        Cerca()


    def __init__(self):
        window = tkinter.Tk()
        window.title(Settings.get_setting_data("title"))
        window_width = Settings.get_setting_data("width")
        window_height = Settings.get_setting_data("height")
        screen_width = window.winfo_screenwidth()
        screen_height = window.winfo_screenheight()
        center_x = int(screen_width / 2 - window_width / 2)
        center_y = int(screen_height / 2 - window_height / 2)
        window.geometry(f'{window_width}x{window_height}+{center_x}+{center_y}')
        window.configure(bg='black')

        # Esplora
        esplora_button = tkinter.Button(
            window,
            text='Esplora',
            command=TIICP.event_test
        )
        esplora_button.pack(
            ipadx=5,
            ipady=5,
            expand=True
        )

        # Cerca
        cerca_button = tkinter.Button(
            window,
            text='Cerca',
            command=TIICP.cerca
        )
        cerca_button.pack(
            ipadx=5,
            ipady=5,
            expand=True
        )

        # Radio
        radio_button = tkinter.Button(
            window,
            text='Radio',
            command=TIICP.event_test
        )
        radio_button.pack(
            ipadx=5,
            ipady=5,
            expand=True
        )

        window.mainloop()

if __name__ == '__main__':
    TIICP()