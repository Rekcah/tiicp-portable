from process.diytojson import Convert
from settings import Settings
import os


class Results:
    @staticmethod
    def results(exp, type):
        diys = []
        prodotti = []
        path_diy = Settings.get_setting_data("diy_files")
        if path_diy[:1] != "/":
            path_diy = path_diy + "/"
        for diy_file in os.listdir(path_diy):
            diy_body = Convert.diytojson(path_diy + diy_file)
            prodotti.append(diy_body['titolo'])
            diys.append(diy_body)

        if type == "product":
            prodotto_presente = False
            prodotti_trovati = []
            for prodotto in prodotti:
                if exp.lower() in prodotto.lower():
                    prodotto_presente = True
                    prodotti_trovati.append(prodotto)

            if prodotto_presente == False:
                return None

            return prodotti_trovati

        elif type == "ingredient":
            ingredient = exp.lower()
            diy_found = []
            for diy in diys:
                if ingredient in diy['elementi']:
                    diy_found.append(diy)
            return diy_found

        elif type == "details":
            prodotto = exp.lower()
            details = ""
            for diy in diys:
                if diy['titolo'] == prodotto:
                    details = diy['elementi']
            return details

        elif type == "pdf":
            prodotto = exp.lower()
            details = ""
            for diy in diys:
                if diy['titolo'] == prodotto:
                    details = diy['pdf']
            return details

        else:
            pass
            # TODO gestione errori

