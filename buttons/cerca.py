import tkinter

from buttons.results import Results
from process.showProductsFromIngredients import showProductsFromIngredients
from process.showProductsFound import showProductsFound
from settings import Settings
from tkinter import ttk

exp = ""

class Cerca():

    def __init__(self):
        cerca_window = tkinter.Toplevel()
        cerca_window.title(Settings.get_setting_data("title"))

        def press(num):
            global exp
            exp = exp + str(num)
            equation.set(exp)

        def clear():
            global exp
            exp = ""
            equation.set(exp)

        def canc():
            global exp
            exp = exp[:(len(exp)) - 1]
            equation.set(exp)

        def search_product():
            global exp
            print("Ricerca del prodotto", equation.get())
            products = Results.results(equation.get(), 'product')
            if equation.get() != '' and equation.get() != ' ':
                exp = ""
                equation.set(exp)
                cerca_window.destroy()
                if products is not None:
                    showProductsFound(products)
                else:
                    pass #TODO gestione nessun prodotto trovato
            else:
                pass #TODO gestione stringa di ricerca vuota

        def search_ingredient():
            global exp
            print("Ricerca dell'ingrediente", equation.get())
            diy_found = Results.results(equation.get(), 'ingredient')
            if equation.get() != '' and equation.get() != ' ':
                exp = ""
                equation.set(exp)
                cerca_window.destroy()
                if diy_found is not None:
                    showProductsFromIngredients(diy_found)
                else:
                    pass #TODO gestione nessun ingrediente trovato
            else:
                pass  # TODO gestione stringa di ricerca vuota
            Results.results(equation.get(), 'ingredient')

        def back():
            global exp
            exp = ""
            equation.set(exp)
            cerca_window.destroy()


        window_width = Settings.get_setting_data("width")
        window_height = Settings.get_setting_data("height")
        screen_width = cerca_window.winfo_screenwidth()
        screen_height = cerca_window.winfo_screenheight()
        center_x = int(screen_width / 2 - window_width / 2)
        center_y = int(screen_height / 2 - window_height / 2)
        cerca_window.geometry(f'{window_width}x{window_height}+{center_x}+{center_y}')
        cerca_window.configure(bg='black')

        equation = tkinter.StringVar()
        Dis_entry = ttk.Entry(cerca_window, state='readonly', textvariable=equation)
        Dis_entry.grid(rowspan=1, columnspan=100, ipadx=1000, ipady=20)

        q = ttk.Button(cerca_window, text='Q', width=6, command=lambda: press('Q'))
        q.grid(row=1, column=0, ipadx=6, ipady=10)

        w = ttk.Button(cerca_window, text='W', width=6, command=lambda: press('W'))
        w.grid(row=1, column=1, ipadx=6, ipady=10)

        E = ttk.Button(cerca_window, text='E', width=6, command=lambda: press('E'))
        E.grid(row=1, column=2, ipadx=6, ipady=10)

        R = ttk.Button(cerca_window, text='R', width=6, command=lambda: press('R'))
        R.grid(row=1, column=3, ipadx=6, ipady=10)

        T = ttk.Button(cerca_window, text='T', width=6, command=lambda: press('T'))
        T.grid(row=1, column=4, ipadx=6, ipady=10)

        Y = ttk.Button(cerca_window, text='Y', width=6, command=lambda: press('Y'))
        Y.grid(row=1, column=5, ipadx=6, ipady=10)

        U = ttk.Button(cerca_window, text='U', width=6, command=lambda: press('U'))
        U.grid(row=1, column=6, ipadx=6, ipady=10)

        I = ttk.Button(cerca_window, text='I', width=6, command=lambda: press('I'))
        I.grid(row=1, column=7, ipadx=6, ipady=10)

        O = ttk.Button(cerca_window, text='O', width=6, command=lambda: press('O'))
        O.grid(row=1, column=8, ipadx=6, ipady=10)

        P = ttk.Button(cerca_window, text='P', width=6, command=lambda: press('P'))
        P.grid(row=1, column=9, ipadx=6, ipady=10)

        clear = ttk.Button(cerca_window, text='Clear', width=6, command=clear)
        clear.grid(row=1, column=13, ipadx=20, ipady=10)

        clear = ttk.Button(cerca_window, text='Canc', width=6, command=canc)
        clear.grid(row=1, column=14, ipadx=20, ipady=10)

        A = ttk.Button(cerca_window, text='A', width=6, command=lambda: press('A'))
        A.grid(row=2, column=0, ipadx=6, ipady=10)

        S = ttk.Button(cerca_window, text='S', width=6, command=lambda: press('S'))
        S.grid(row=2, column=1, ipadx=6, ipady=10)

        D = ttk.Button(cerca_window, text='D', width=6, command=lambda: press('D'))
        D.grid(row=2, column=2, ipadx=6, ipady=10)

        F = ttk.Button(cerca_window, text='F', width=6, command=lambda: press('F'))
        F.grid(row=2, column=3, ipadx=6, ipady=10)

        G = ttk.Button(cerca_window, text='G', width=6, command=lambda: press('G'))
        G.grid(row=2, column=4, ipadx=6, ipady=10)

        H = ttk.Button(cerca_window, text='H', width=6, command=lambda: press('H'))
        H.grid(row=2, column=5, ipadx=6, ipady=10)

        J = ttk.Button(cerca_window, text='J', width=6, command=lambda: press('J'))
        J.grid(row=2, column=6, ipadx=6, ipady=10)

        K = ttk.Button(cerca_window, text='K', width=6, command=lambda: press('K'))
        K.grid(row=2, column=7, ipadx=6, ipady=10)

        L = ttk.Button(cerca_window, text='L', width=6, command=lambda: press('L'))
        L.grid(row=2, column=8, ipadx=6, ipady=10)

        cerca_prodotto = ttk.Button(cerca_window, text='Cerca prodotto', width=6, command=search_product)
        cerca_prodotto.grid(row=4, columnspan=80, ipadx=80, ipady=10)

        cerca_ingrediente = ttk.Button(cerca_window, text='Cerca ingrediente', width=6, command=search_ingredient)
        cerca_ingrediente.grid(row=4, columnspan=100, ipadx=80, ipady=10)

        Z = ttk.Button(cerca_window, text='Z', width=6, command=lambda: press('Z'))
        Z.grid(row=3, column=0, ipadx=6, ipady=10)

        X = ttk.Button(cerca_window, text='X', width=6, command=lambda: press('X'))
        X.grid(row=3, column=1, ipadx=6, ipady=10)

        C = ttk.Button(cerca_window, text='C', width=6, command=lambda: press('C'))
        C.grid(row=3, column=2, ipadx=6, ipady=10)

        V = ttk.Button(cerca_window, text='V', width=6, command=lambda: press('V'))
        V.grid(row=3, column=3, ipadx=6, ipady=10)

        B = ttk.Button(cerca_window, text='B', width=6, command=lambda: press('B'))
        B.grid(row=3, column=4, ipadx=6, ipady=10)

        N = ttk.Button(cerca_window, text='N', width=6, command=lambda: press('N'))
        N.grid(row=3, column=5, ipadx=6, ipady=10)

        M = ttk.Button(cerca_window, text='M', width=6, command=lambda: press('M'))
        M.grid(row=3, column=6, ipadx=6, ipady=10)

        space = ttk.Button(cerca_window, text='Spazio', width=6, command=lambda: press(' '))
        space.grid(row=4, columnspan=14, ipadx=160, ipady=10)

        back = ttk.Button(cerca_window, text='Indietro', width=6, command=back)
        back.grid(row=4, column=1, ipadx=30, ipady=10)


        cerca_window.mainloop()

